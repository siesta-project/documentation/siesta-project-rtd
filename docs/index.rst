.. _Siesta Project homepage:

The SIESTA project
==================

This is the reference documentation for the SIESTA suite of programs,
utilities, and data.


* `Siesta Documentation
  <https://docs.siesta-project.org/projects/siesta>`_.
* `AiiDA-Siesta Documentation
  <https://docs.siesta-project.org/projects/aiida-siesta>`_.
* `Documentation for the ATOM pseudopotential-generation program
  <https://docs.siesta-project.org/projects/atom>`_.

Acknowledgements
----------------

The SIESTA project is supported by the MaX European
Centre of Excellence, and by the Spanish State Research Agency.

.. image:: source/sponsors/max.png
   :target: http://www.max-centre.eu/
   :width: 155px


